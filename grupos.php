<?php session_start();
$errores = '';
$creado = '';
$uploads = "uploads";

//if(!isset($_SESSION['usuarios'])){
//    header('Location: index.php');
//}

if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    $grupo = filter_var(strtolower($_POST['grupo']), FILTER_SANITIZE_STRING);
    $descripcion_grupo = filter_var($_POST['Descripcion_grupo'], FILTER_SANITIZE_STRING);
    $portada = time().$_FILES['Imajen']['name'];
        
    if(empty($grupo) or empty($descripcion_grupo)){
       $errores .= 'Todos los campos deben estar llenos';
    }
   
    if(strlen($grupo) < 4){
        $errores .= "El grupo debe tener minimo 5 caracteres";
    }
        
    require 'conexion.php';
    //verifica que el nombre del grupo no exista.
    
     $statement = $conexion -> prepare("SELECT NombreGrupo FROM grupos WHERE NombreGrupo = ? LIMIT 1");
     $statement -> bind_param('s',$grupo);
     $statement -> execute();
     $result= $statement -> fetch();
        
    if($result == true){
        $errores .= 'Ya existe un grupo con este nombre';
    }
    
            
        if($errores == ''){
            //verifica que exista una foto y realiza el movimiento de la imajen a una carpeta
            if(empty($_FILES['Imajen']['name'])){
                     $errores .= '<br> No ahy ningun archivo, debe subir la foto para la portada del grupo </br>';
            }else{
                //valida que el archivo sea jpeg png pjpeg
                if($_FILES['Imajen']['type'] == 'image/jpeg' or $_FILES['Imajen']['type'] == 'image/png' or $_FILES['Imajen']['type'] == 'image/pjpeg'){

                    $portada = time().$_FILES['Imajen']['name'];
                    move_uploaded_file($_FILES['Imajen']['tmp_name'], "$uploads/$portada");

                }else{
                     $errores .= '<br> Los formatos no son compatibles </br>';
                }
            }
            
            $statement = $conexion -> prepare('INSERT INTO grupos(NombreGrupo, portada, descripcion) VALUES(?,?,?)');
            $statement -> bind_param('sss', $grupo, $portada, $descripcion_grupo);
            $statement -> execute();
            
            $creado .= "El grupo se ha creado";
        }
}

require '\views\crearGrupo.view.php';
?>