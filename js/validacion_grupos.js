(function(){
   var formulario = document.formulario_grupo,
       elementos = formulario.elements;
    
    
    
    //funciones
    var validarInputs = function(){
        for(var i = 0; i < elementos.length; i++){
            if(elementos[i].type == 'text' || elementos[i].type == 'file' || elementos[i].type == 'textarea'){
                if(elementos[i].value == ''){
                    alert("El campo "+elementos[i].name+" esta vacio");
                    elementos[i].className = elementos[i].className + " error";
                    return false;
                }else{
                    elementos[i].className = elementos[i].className.replace("error", "");
                }
            }
        }
        e.preventDefault();
    };
    
    var validar = function(e){
      if(!validarInputs()){
          e.preventDefault();
      }
    };
        
    //funciones blur
    var focusInput = function(){
        this.parentElement.children[0].className = "active";
        this.parentElement.children[0].className = this.parentElement.children[0].className.replace("error", "");
    }
    
    var blurInput = function(){
        if(this.value == 0){
            this.parentElement.children[0].className = "active";
            this.parentElement.children[0].className = this.parentElement.children[0].className +" error";
        }
    }
    
    
    var focusText = function(){
        this.parentElement.children[0].className = "activeText";
        this.parentElement.children[0].className = this.parentElement.children[0].className.replace('error', '');
    }
    
    var blurText = function(){
        this.parentElement.children[0].className = "activeText";
        this.parentElement.children[0].className = this.parentElement.children[0].className +" error";
    }
    
        
    //eventos
    var enviar = formulario.addEventListener('submit', validar);
    
    for(var i = 0; i<elementos.length; i++){
        if(elementos[i].type == 'text' || elementos[i].type == 'file'){
            elementos[i].addEventListener('blur', blurInput);
            elementos[i].addEventListener('focus', focusInput);
        }else if(elementos[i].type == 'textarea'){
            elementos[i].addEventListener('blur', blurText);
            elementos[i].addEventListener('focus', focusText);
        }
    }
    
}())