<!DOCTYPE html>

<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/estilos.css">
    </head>
    <body>
       
       <section class="container-from">
        <div class="contenedorForm">
          
            <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" name="formulario_grupo"  enctype="multipart/form-data" class="formulario">
               <div class="div-form ">
                <input type="text" class="" name="grupo" placeholder="Nombre Grupo:" autofocus>
               </div>
               <div class="div-form">
                <input type="file" class="file" name="Imajen" placeholder="Portada del grupo">
               </div>
               <div class="div-form">
                <textarea name="Descripcion_grupo" id="" class="" cols="30" rows="10"  placeholder="Descripcion general del grupo:"></textarea>
               </div>
                <input type="submit" value="crear" class="btn-grupo">
            </form>

                <?php if(!empty($errores)): ?>
                   <div class="errores_formulario">
                       <?php echo $errores; ?>
                   </div>
                <?php elseif(!empty($creado)): ?>
                   <div class="creado">
                       <?php echo $creado; ?>
                   </div>
                <?php endif; ?>
        </div>
       </section>
        
        <script src="js/validacion_grupos.js"></script>
    </body>
</html>