<!DOCTYPE html>

<html lang="es">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximu-scale=1, minimun-scale=1">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/estilos.css">
        <link rel="stylesheet" href="css/normalize.css">
        
        <link href="https://file.myfontastic.com/FvgGGdEJMc7KQn8URFBPDo/icons.css" rel="stylesheet">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        
    </head>    
    <body class="Registro">
       <!--- barra de navegacion ---->      
    <header>
        <input type="checkbox" id="btn-menu">
          <label for="btn-menu" class="icon-menu"></label>
            <nav class="menu">
              <ul>
                <li class="sub-menu"><a class="categorias" href="#">Cursos<span class="icon-angle-down"></span></a>
                    <ul>
                        <li><a class="categorias" href="views/programacionEstruc.view.php">Programación estructurada</a></li>
                        <li><a class="categorias" href="views/CSharp.view.php">Programación c#</a></li>
                        <li><a class="categorias" href="views/programacionJava.view.php">Programación java</a></li>
                        <li><a class="categorias" href="views/BaseDatos.view.php">Bases de datos</a></li>
                        <li><a class="categorias" href="views/redesComput.view.php">Redes de computadora</a></li>
                        <li><a class="categorias" href="views/mineriaDatos.view.php">Minería de datos</a></li>
                        <li class="li-hardware"><a class="categorias" href="views/hardware.view.php">Hardware( Arquitectura de computadoras)</a></li>
                        <li><a class="categorias" href="views/seguridadHaking.view.php">Seguridad y hacking ético</a></li>	
                    </ul>
                </li>
                <li class="Blog"><a class="categorias" href="views/blog.view.php">Blog</a></li>
                    <?php if(!isset($_SESSION['usuario'])): ?>
                        <li class="login-nav"><a class="categorias" href="#">Iniciar Sesion</a></li>
                        <li class="Blog"><a class="categorias" href="registro.php">Registro</a></li>
                    <?php else: ?>
                        <li class="blog"><a href="cerrar.php">Salir</a></li>
                        <li class="blog" ><a href="!#" ><?php echo $_SESSION['usuario']?><span class="icon-basket"></span></a>
                   <?php endif; ?>
                    <li class="Buscador">
                         <a href="#" class="icon-search"></a>
                         <input type="text"class="input">
                    </li>
             </ul>
         </nav>
       </header>
        
        <h1>Registro</h1>
        
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" class="form">
           <h2>Crea una Cuenta</h2>
           
           <div class="contenedor">
            <input type="text" class="media" placeholder="Nombre" name="nombre"
                   value="<?php echo set_value_input(array(),"nombre","nombre");?>" >
            <input type="text" class="media" placeholder="Usuario" name="usuario" 
                    value="<?php echo set_value_input(array(),"usuario","usuario");?>" >
            <input type="text" placeholder="Apellidos" name="apellido"
                    value="<?php echo set_value_input(array(),"apellido","apellido"); ?>" >
            <input type="email" placeholder="Correo" name="correo" 
                    value="<?php echo set_value_input(array(),"correo","correo");?>" >
            <input type="password" placeholder="Contraseña" name="paswd" >
            <input type="password" placeholder="Repita la contraseña" name="paswd2" >
            <label for="fecha">Fecha de nacimiento</label>
            <input type="date" class="media media-m" name="fecha">
            <textarea name="perfil" id="" cols="30" rows="10" value="<?php echo set_value_input(array(),"perfil","perfil");?>"></textarea>
            <input type="submit" value="Registrate" class="btn media-m">
           </div>    
        </form>
            
            <?php if(!empty($errores)): ?>
				<?php echo $errores; ?>
            <?php elseif(!empty($registrado)): ?>
                <?php echo $registrado; ?>
			<?php endif; ?>
            
            
        <script src="js/menu.js"></script>    
    </body>
</html>        