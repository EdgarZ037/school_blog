<html lang="es">

<head>
<title></title>
<meta charset="utf-8" />
<link rel="stylesheet" href="../css/estilosBlog.css">
<link rel="stylesheet" href="../css/normalize.css">
<link href="https://file.myfontastic.com/FvgGGdEJMc7KQn8URFBPDo/icons.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>

<body>
       
<!--- barra de navegacion ---->      
<header>
    <input type="checkbox" id="btn-menu">
      <label for="btn-menu" class="icon-menu"></label>
        <nav class="menu">
          <ul>
            <li class="sub-menu"><a class="categorias" href="#">Cursos<span class="icon-angle-down"></span></a>
                <ul>
                    <li><a class="categorias" href="programacionEstruc.view.php">Programación estructurada</a></li>
                    <li><a class="categorias" href="CSharp.view.php">Programación c#</a></li>
                    <li><a class="categorias" href="programacionJava.view.php">Programación java</a></li>
                    <li><a class="categorias" href="BaseDatos.view.php">Bases de datos</a></li>
                    <li><a class="categorias" href="redesComput.view.php">Redes de computadora</a></li>
                    <li><a class="categorias" href="mineriaDatos.view.php">Minería de datos</a></li>
                    <li class="li-hardware"><a class="categorias" href="hardware.view.php">Hardware( Arquitectura de computadoras)</a></li>
                    <li><a class="categorias" href="seguridadHaking.view.php">Seguridad y hacking ético</a></li>	
                </ul>
            </li>
            <li class="Blog"><a class="categorias" href="blog.view.php">Blog</a></li>
                <?php if(!isset($_SESSION['usuario'])): ?>
                    <li class="login-nav"><a class="categorias" href="#">Iniciar Sesion</a></li>
                    <li class="Blog"><a class="categorias" href="../registro.php">Registro</a></li>
                <?php else: ?>
                    <li class="blog"><a href="cerrar.php">Salir</a></li>
                    <li class="blog" ><a href="!#" ><?php echo $_SESSION['usuario']?><span class="icon-basket"></span></a>
               <?php endif; ?>
                <li class="Buscador">
                     <a href="#" class="icon-search"></a>
                     <input type="text"class="input">
                </li>
         </ul>
     </nav>
   </header>
        
   <main class="contenedor-blog">
         <div class="contenedor--img">
              <a href=""><img src="../img/base-de-datos-curso.png" class="info--img">
                         <div class="descripcion"><p>brian joto</p></div>
              </a>
         </div> 
         <div class="contenedor--img">
              <a href=""><img src="../img/base-de-datos-curso.png" class="info--img">
                         <div class="descripcion"><p>brian joto</p></div>
              </a>
         </div>           
         <div class="contenedor--img">
              <a href=""><img src="../img/base-de-datos-curso.png" class="info--img">
                         <div class="descripcion"><p>brian joto</p></div>
              </a>
         </div>    
         <div class="contenedor--img">
              <a href=""><img src="../img/base-de-datos-curso.png" class="info--img">
                         <div class="descripcion"><p>brian joto</p></div>
              </a>
         </div>        
   </main>
      
    <footer class="footer">
           <div class="contenedor-footer">
               <a href="#" class="iconos"><span class="icon-facebook"></span></a>
               <a href="#" class="iconos"><span class="icon-twitter"></span></a>
               <a href="#" class="iconos"><span class="icon-instagram"></span></a>
           </div>
               <p class="copy">&copy; 2016 todos los derechos reservados</p>
    </footer>
    
     <script src="../js/menu.js"></script>
</body>
</html>