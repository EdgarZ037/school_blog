<html lang="es">

<head>
<title></title>
<meta charset="utf-8" />
<link rel="stylesheet" href="../css/estilos.css">
<link rel="stylesheet" href="../css/normalize.css">
<link href="https://file.myfontastic.com/FvgGGdEJMc7KQn8URFBPDo/icons.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>

<body>
       
<!--- barra de navegacion ---->      
<header>
    <input type="checkbox" id="btn-menu">
      <label for="btn-menu" class="icon-menu"></label>
        <nav class="menu">
          <ul>
            <li class="sub-menu"><a class="categorias" href="#">Cursos<span class="icon-angle-down"></span></a>
                <ul>
                    <li><a class="categorias" href="programacionEstruc.view.php">Programación estructurada</a></li>
                    <li><a class="categorias" href="CSharp.view.php">Programación c#</a></li>
                    <li><a class="categorias" href="programacionJava.view.php">Programación java</a></li>
                    <li><a class="categorias" href="BaseDatos.view.php">Bases de datos</a></li>
                    <li><a class="categorias" href="redesComput.view.php">Redes de computadora</a></li>
                    <li><a class="categorias" href="mineriaDatos.view.php">Minería de datos</a></li>
                    <li class="li-hardware"><a class="categorias" href="hardware.view.php">Hardware( Arquitectura de computadoras)</a></li>
                    <li><a class="categorias" href="seguridadHaking.view.php">Seguridad y hacking ético</a></li>	
                </ul>
            </li>
            <li class="Blog"><a class="categorias" href="blog.view.php">Blog</a></li>
                <?php if(!isset($_SESSION['usuario'])): ?>
                    <li class="login-nav"><a class="categorias" href="#">Iniciar Sesion</a></li>
                    <li class="Blog"><a class="categorias" href="../registro.php">Registro</a></li>
                <?php else: ?>
                    <li class="blog"><a href="cerrar.php">Salir</a></li>
                    <li class="blog" ><a href="!#" ><?php echo $_SESSION['usuario']?><span class="icon-basket"></span></a>
               <?php endif; ?>
                <li class="Buscador">
                     <a href="#" class="icon-search"></a>
                     <input type="text"class="input">
                </li>
         </ul>
     </nav>
   </header>
        
  <main class="contenedor-main">
        <section class="info">
             <article class="info-columna">
                     <a href="#" class="cursos"><img src="../img/curso-hardware.jpg"class="info__img">
                     <h2 class="info__titulo">Curso Hardware#1</h2></a>
                     <p class="info-texto">en este curso aprenderemos programacion en java basica y programacion orientada a objetos</p>
              </article>
          
              <article class="info-columna">
                     <a href="#" class="cursos"><img src="../img/curso-hardware.jpg"class="info__img">
                     <h2 class="info__titulo">Curso Hardware#2</h2></a>
                     <p class="info-texto">en este curso aprenderemos programacion en java basica y programacion orientada a objetos</p>
              </article>
          
              <article class="info-columna">
                     <a href="#" class="cursos"><img src="../img/curso-hardware.jpg" alt="" class="info__img">
                     <h2 class="info__titulo">Curso Hardware#3</h2></a>
                     <p class="info-texto">en este curso aprenderemos programacion en java basica y programacion orientada a objetos</p>
              </article>
        </section>
        
        <section class="info">
             <article class="info-columna">
                    <a href="#" class="cursos"><img src="../img/curso-hardware.jpg"class="info__img">
                     <h2 class="info__titulo">Curso Hardware#4</h2></a>
                     <p class="info-texto">en este curso aprenderemos programacion en java basica y programacion orientada a objetos</p>
              </article>
          
              <article class="info-columna">
                    <a href="#" class="cursos"><img src="../img/curso-hardware.jpg"class="info__img">
                     <h2 class="info__titulo">Curso Hardware#5</h2></a>
                     <p class="info-texto">en este curso aprenderemos programacion en java basica y programacion orientada a objetos</p>
              </article>
          
              <article class="info-columna">
                    <a href="#" class="cursos"><img src="../img/curso-hardware.jpg" alt="" class="info__img">
                     <h2 class="info__titulo">Curso Hardware#6</h2></a>
                     <p class="info-texto">en este curso aprenderemos programacion en java basica y programacion orientada a objetos</p> 
              </article>
        </section>
   </main>
    
    <footer class="footer">
           <div class="contenedor-footer">
               <a href="#" class="iconos"><span class="icon-facebook"></span></a>
               <a href="#" class="iconos"><span class="icon-twitter"></span></a>
               <a href="#" class="iconos"><span class="icon-instagram"></span></a>
           </div>
               <p class="copy">&copy; 2016 todos los derechos reservados</p>
    </footer>
    
      <script src="../js/menu.js"></script>
</body>
</html>