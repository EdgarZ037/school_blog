<?php 

include 'funciones.php';

$errores = '';
$registrado = '';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $usuario = filter_var(trim(strtolower($_POST['usuario'])), FILTER_SANITIZE_STRING);
    $correo = filter_var(strtolower($_POST['correo']), FILTER_SANITIZE_EMAIL);
    $nombre = filter_var(strtolower($_POST['nombre']), FILTER_SANITIZE_STRING);
    $apellido = filter_var(strtolower($_POST['apellido']), FILTER_SANITIZE_STRING);
    $passwd = filter_var($_POST['paswd'], FILTER_SANITIZE_STRING);
    $passwd2 = filter_var($_POST['paswd2'], FILTER_SANITIZE_STRING);
    $fecha = date($_POST['fecha']);
    $perfilUser = filter_var($_POST['perfil'], FILTER_SANITIZE_STRING);
    $tipoAcceso = 'alumno';
        
    //confirmar que ningun campo este vacio en el registro
    if(empty($usuario) or empty($correo) or empty($nombre) or empty($apellido) or empty($passwd) or empty($passwd2) or empty($perfilUser)){
        $errores .= 'Ningun campo debe ir vacio </br>';
    }
       //se pregunta que los siguientes campos tengan un patron valido
       if (filter_var(trim($correo), FILTER_VALIDATE_EMAIL) == false) {
			$errores .= ' El Correo ingresado no tiene un formato valido </br>';
		}
       if (strlen($usuario) <= 4 ){
            $errores .= "Su usuario debe tener 5 caracteres como minimo </br>";
        } 
       if(preg_match("[a-zA-Z]",$nombre) == false && (strlen($nombre) <= 4 )){
            $errores .= "Por favor ingrese su nombre</br>";
        }
       if(preg_match("[a-zA-Z]",$apellido) == false && (strlen($apellido) <= 4 )){
            $errores .= "Por favor ingrese sus apellidos</br>";
        }
            
        //se consulta la BD para ver si este usuario ya existe, para asi no tener mas de un usuario repetido
        require 'conexion.php';
    
        $statement = $conexion -> prepare("SELECT Usuario FROM usuarios WHERE Usuario = ? LIMIT 1");
        $statement -> bind_param('s',$usuario);
        $statement -> execute();
        $result = $statement -> fetch();
    
       if($result == true){
           $errores .= 'Este usuario esta en uso';
       }
    
        //Verificar que las contraseñas coincidan
       $passwd = hash('sha512', $passwd);
       $passwd2 = hash('sha512', $passwd2);
        
       if($passwd !== $passwd2){
           $errores .= 'Las contraseñas no coinciden';
       }
       
    //inserciones en la base de datos
    if($errores == ''){
        
        $statement = $conexion -> prepare("INSERT INTO usuarios(Usuario, Nombre, Apellido, passwd, FechaNacimiento, Perfil, TipoAcceso)
                                                      VALUES (?,?,?,?,?,?,?)");
        $statement -> bind_param('sssssss', $usuario, $nombre, $apellido, $passwd, $fecha, $perfilUser, $tipoAcceso);
        $statement -> execute();
               
        $registrado .= 'Enviado correctamente';
    }
}

require '\views\registro.view.php';
?>